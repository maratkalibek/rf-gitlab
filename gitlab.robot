*** Settings ***
Documentation     A test suite with a single test for valid login.
...
...               This test has a workflow that is created using keywords in
...               the imported resource file.
Resource          resource.robot

*** Test Cases ***
Valid Login
    Open Browser To Login Page
    Input Username    ${VALID_USER}
    Input Password    ${VALID_PASSWORD}
    Submit Credentials
    Welcome Page Should Be Open
    [Teardown]    Close Browser

Invalid Login
    Open Browser To Login Page
    Input Username    unreal_user
    Input Password    unreal_password
    Submit Credentials
    Invalid Login Info Alert Should Be Displayed
    [Teardown]    Close Browser

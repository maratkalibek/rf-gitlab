This is demo using robotframework to run regression tests on gitlab.

It is simple scenarios for testing valid/invalid login to gitlab. see https://gitlab.com/gitlab-org/gitlab-ce/issues/14355


1. sudo apt-get install python-pip
2. sudo apt-get install firefox (if not installed)
3. sudo pip install robotframework
4. sudo pip install robotframework-selenium2library

5. pybot gitlab.robot

6. see results in log.html


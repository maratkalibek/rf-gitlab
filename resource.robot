*** Settings ***
Documentation     A resource file with reusable keywords and variables.
...
...               The system specific keywords created here form our own
...               domain specific language. They utilize keywords provided
...               by the imported Selenium2Library.
Library           Selenium2Library

*** Variables ***
${SERVER}         gitlab.com
${BROWSER}        Firefox
${DELAY}          0
${VALID USER}     maratkalibek_test
${VALID PASSWORD}    HelloWorld123$
${LOGIN URL}      http://${SERVER}/users/sign_in

*** Keywords ***
Open Browser To Login Page
    Open Browser    ${LOGIN URL}    ${BROWSER}
    Maximize Browser Window
    Set Selenium Speed    ${DELAY}
    Login Page Should Be Open

Login Page Should Be Open
    Title Should Be    Sign in · GitLab

Go To Login Page
    Go To    ${LOGIN URL}
    Login Page Should Be Open

Input Username
    [Arguments]    ${username}
    Input Text    user_login    ${username}

Input Password
    [Arguments]    ${password}
    Input Text    user_password    ${password}

Submit Credentials
    Click Element    name=commit

Welcome Page Should Be Open
    Element Should Be Visible    xpath=//a[@class='sidebar-user']

Invalid Login Info Alert Should Be Displayed
    Element Text Should Be    xpath=//div[@class='flash-alert']    Invalid login or password.

FROM ubuntu:14.04

RUN sudo apt-get update
RUN sudo apt-get install -y python-pip
RUN sudo apt-get install -y firefox
RUN sudo apt-get install -y xvfb
RUN sudo pip install robotframework
RUN sudo pip install robotframework-selenium2library

COPY robotframework robotframework
RUN chmod +x robotframework
